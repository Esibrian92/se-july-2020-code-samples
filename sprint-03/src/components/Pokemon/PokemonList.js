import { Link, useRouteMatch } from "react-router-dom";
import { ListGroup } from "react-bootstrap";
import { getIdFromUrl } from "../../util";

export const PokemonList = ({ pokemon }) => {
  const match = useRouteMatch();
  return (
    <>
      <ListGroup>
        {pokemon.map((p) => (
          <Link key={p.url} to={`${match.url}/${getIdFromUrl(p.url)}`}>
            <ListGroup.Item>{p.name}</ListGroup.Item>
          </Link>
        ))}
      </ListGroup>
    </>
  );
};
