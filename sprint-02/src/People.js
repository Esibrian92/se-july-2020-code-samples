import { Route, Link, Switch, useRouteMatch, useParams, Redirect } from "react-router-dom";

const Person = ({ people }) => {
  const params = useParams();
  // const person = people.find((p) => p.id === Number.parseInt(params.personId, 10));
  const person = people.find((p) => p.id === params.personId);
  if (!person) {
    return <Redirect to="/people" />;
  }
  return (
    <>
      <h1>{person.first}</h1>
    </>
  );
};

const People = ({ people }) => {
  const match = useRouteMatch();
  return (
    <>
      <Switch>
        <Route path={`${match.path}/:personId`}>
          <Person people={people} />
        </Route>
        <Route path={match.path}>
          <ul>
            {people.map((person) => (
              <li key={person.id}>
                <Link to={`${match.path}/${person.id}`}>{person.first}</Link>
              </li>
            ))}
          </ul>
        </Route>
      </Switch>
    </>
  );
};

export default People;
