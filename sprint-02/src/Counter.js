import { useState } from "react";
import { useHistory, useRouteMatch, useLocation } from "react-router-dom";

const Counter = ({ name, age, ...otherProps }) => {
  // const { name, age } = props;
  const history = useHistory();
  const match = useRouteMatch();
  const location = useLocation();

  console.log("Counter Props", otherProps, { history, match, location });

  let [count, setCount] = useState(0);
  const [count2, setCount2] = useState(2);
  const [count3, setCount3] = useState(3);
  const [person, setPerson] = useState({
    name,
    age,
  });

  const handleInc = () => {
    setCount((cnt) => {
      return cnt + 1;
    });
    setCount((cnt) => cnt + 1);
  };
  const handleDec = () => setCount(count - 1);
  const thisIsBad = () => {
    count += 2;
  };

  const handleDoublePersonName = () => {
    setPerson((currentPerson) => {
      return {
        ...currentPerson,
        age: currentPerson.age * 2,
      };
    });
  };

  const handleNameChange = (event) => {
    setPerson((pers) => ({
      ...pers,
      name: event.target.value,
    }));
  };

  return (
    <>
      <h1>Counter Bruh</h1>
      <p>{count}</p>
      <button onClick={handleInc}>+</button>
      <button onClick={handleDec}>-</button>
      <button onClick={thisIsBad}>Dont Do This</button>
      <hr />

      <p>{count2}</p>
      <button onClick={() => setCount2(count2 + 1)}>+</button>
      <button onClick={() => setCount2(count2 - 1)}>-</button>
      <hr />

      <p>{count3}</p>
      <button onClick={() => setCount3(count3 + 1)}>+</button>
      <button onClick={() => setCount3(count3 - 1)}>-</button>
      <hr />
      <p>{`${person.name} is ${person.age} years old`}</p>
      <button onClick={handleDoublePersonName}>Double Persons Name</button>

      <input value={person.name} onChange={handleNameChange} />
    </>
  );
};

export const DumbCounter = Counter;

let i = 0;
export const getId = () => ++i;

export const setCounterTo = (num) => {
  i = num;
};

export default Counter;
// export default withRouter(Counter);
